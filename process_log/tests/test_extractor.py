import os
import sys
from datetime import datetime
from unittest.mock import patch, Mock

from pyspark.sql import SparkSession
from pytest import mark, raises as raises_pytest

from process_log import ExtractInfo
from process_log.exceptions import ProcessLogException


class TestExtractor:
    def spark(self):
        return SparkSession.builder.master("local").appName("PythonChallenge").getOrCreate()

    def test_constructor_default(self):
        with patch('process_log.extractor.SparkSession') as SparkSession:
            extractor = ExtractInfo()
        assert all([extractor.path == os.path.abspath(sys.argv[0]),
                    SparkSession.builder.master().appName().getOrCreate.called])

    def test_extract_valids_files(self):
        with patch('process_log.extractor.os.listdir', return_value=['a.log', 'b.log', 'nolog', 'part']):
            with patch('process_log.extractor.os.path.join', lambda x, y: y):
                with patch('process_log.extractor.SparkSession'):
                    with patch('process_log.extractor.os.path.isdir', return_value=True):
                        assert ExtractInfo()._extract_valids_files() == ['a.log', 'b.log']

    def test_extract_valids_files_not_dir(self):
        with patch('process_log.extractor.os.listdir', return_value=['a.log', 'b.log', 'nolog', 'part']):
            with patch('process_log.extractor.os.path.join', lambda x, y: y):
                with patch('process_log.extractor.SparkSession'):
                    with patch('process_log.extractor.os.path.isdir', return_value=False):
                        assert ExtractInfo()._extract_valids_files() == []

    @mark.parametrize('files, called', [
        [[], False],
        [['a.log', 'b.log'], True]
    ])
    def test_charge_logs(self, files, called):
        with patch('process_log.extractor.SparkSession'):
            with patch('process_log.extractor.ExtractInfo._extract_valids_files', return_value=files):
                with patch('process_log.extractor.ExtractInfo._prepare_dataframe') as prepare_dataframe:
                    extractor = ExtractInfo()
                    extractor.read_logs_and_build_dataframe()

        assert all([extractor.spark.read.text.called is called,
                    prepare_dataframe.called is called])

    @mark.parametrize('datetime_inf, datetime_sup, count', [
        [None, None, 4],
        [datetime(2016, 6, 8, 1), None, 3],
        [datetime(2016, 6, 8, 1), datetime(2019, 12, 21, 1), 2],
    ])
    def test_prepare_dataframe(self, datetime_inf, datetime_sup, count):
        rows = [['1559989891 gandalf vader'],
                ['1559989905 fernando cortes'],
                ['1591612278 juan tesla'],
                ['1433759478 feyman einstein']]
        spark = self.spark()
        dataframe = spark.createDataFrame(rows)

        extractor = ExtractInfo(spark=spark)
        extractor.df = dataframe
        extractor._prepare_dataframe(datetime_inf, datetime_sup)
        assert extractor.df.count() == count

    def test_decorator_apply(self):
        extractor = ExtractInfo(spark=Mock())
        with raises_pytest(ProcessLogException):
            extractor.extract_list_hosts_connected_to('a')
        with raises_pytest(ProcessLogException):
            extractor.extract_list_hosts_received_connection_to('a')
        with raises_pytest(ProcessLogException):
            extractor.hostname_more_conections('a')

    def test_extract_list_hosts_connected_to(self):
        rows = [['1559989891 gandalf vader'],
                ['1559989905 fernando gandalf'],
                ['1591612278 gandalf tesla'],
                ['1433759478 einstein tesla']]
        spark = self.spark()
        dataframe = spark.createDataFrame(rows)

        extractor = ExtractInfo(spark=spark)
        extractor.df = dataframe
        extractor._prepare_dataframe()
        result = extractor.extract_list_hosts_connected_to('tesla')
        assert all([len(result) == 2,
                    all([el in result for el in ['gandalf', 'einstein']])])

    def test_extract_list_hosts_received_conexion_to(self):
        rows = [['1559989891 gandalf vader'],
                ['1559989905 fernando gandalf'],
                ['1591612278 gandalf tesla'],
                ['1433759478 gandalf einstein']]
        spark = self.spark()
        dataframe = spark.createDataFrame(rows)

        extractor = ExtractInfo(spark=spark)
        extractor.df = dataframe
        extractor._prepare_dataframe()
        result = extractor.extract_list_hosts_received_connection_to('gandalf')
        assert all([len(result) == 3,
                    all([el in result for el in ['vader', 'tesla', 'einstein']])])

    def test_hostname_more_conections(self):
        rows = [['1559989891 gandalf vader'],
                ['1559989905 fernando gandalf'],
                ['1591612278 gandalf tesla'],
                ['1591612278 tesla fernando'],
                ['1433759478 gandalf einstein'],
                ['1433759478 tesla fernando'],
                ['1433759478 tesla einstein'],
                ['1433759478 tesla barbara']]
        spark = self.spark()
        dataframe = spark.createDataFrame(rows)

        extractor = ExtractInfo(spark=spark)
        extractor.df = dataframe
        extractor._prepare_dataframe()
        assert extractor.hostname_more_conections() == 'tesla'
