import os
import sys
from datetime import datetime
from typing import List

from pyspark.sql import SparkSession  # type: ignore
from pyspark.sql import functions as F  # type: ignore

from process_log.decorator import ensure_data


class ExtractInfo:
    """
    Clase encargada de obtener la información de los logs y devolver información sobre las conexiones

    """

    def __init__(self, path: str = None, spark: SparkSession = None):
        """
        Constructor encargado de inicializar el sistema.
        Args:
            path: ruta donde están los logs, por defecto la ruta en la que se esté ejecutando
            spark: Si se desea utilizar una sesión de spark externa, en caso de que no se especifique se levanta o se captura una local
        """
        self.path = os.path.abspath(sys.argv[0]) if path is None else path
        self.spark = SparkSession.builder.master("local").appName("PythonChallenge").getOrCreate() if spark is None else spark
        self.df = None

    def _extract_valids_files(self) -> List[str]:
        """
        Método privado encargado de obtener solamente aquellos ficheros que son de extensión log. Si los logs tuvieran la hora y minuto,
        se podría en esta función ampliar, para así optimizar la lectura, se ha supuesto que no para ser lo más genérico posible
        Args:
        Returns: lista de strings con todos los nombres de ficheros

        """
        if not os.path.isdir(self.path):
            return []
        return [os.path.join(self.path, file) for file in os.listdir(self.path) if file.endswith('.log')]

    def _prepare_dataframe(self, datetime_inf: datetime = None, datetime_sup: datetime = None) -> None:
        """
        Método privado encargado de adaptar el dataframe inicial a uno con las tres columnas necesarias para trabajar timestamp, connector y receiver
        Filtra por datetime_inf si lo hubiera y tambien por datetime_sup, el dataframe resultante es cacheado para acelerar las consultas posteriores

        Args:
            datetime_inf: cota superior del datetime de la conexión
            datetime_sup: cota inferior del datetime de la conexión

        Returns:

        """
        if self.df is not None:
            timestamp_inf = None if datetime_inf is None else int(datetime.timestamp(datetime_inf))
            timestamp_sup = None if datetime_sup is None else int(datetime.timestamp(datetime_sup))
            column_ini = self.df.columns[0]

            self.df = self.df.withColumn(
                'timestamp', F.split(self.df[column_ini], ' ')[0]).withColumn(
                'connector', F.split(self.df[column_ini], ' ')[1]).withColumn(
                'receiver', F.split(self.df[column_ini], ' ')[2]).drop(column_ini)

            if timestamp_inf is not None:
                self.df = self.df.where(timestamp_inf <= F.col('timestamp'))
            if timestamp_sup is not None:
                self.df = self.df.where(F.col('timestamp') <= timestamp_sup)
            self.df.cache()

    def read_logs_and_build_dataframe(self, datetime_inf: datetime = None, datetime_sup: datetime = None) -> None:
        """
        Método privado encargado de leer los logs y crear el dataframe, en caso de que no exista datos no crea el dataframe
        Si no tiene datetime_inf, deberá cargar todos los logs hasta el datetime_sup, lo mismo si no hay datetime_sup
        Args:
            datetime_inf: fecha inferior
            datetime_sup: fecha superior

        Returns: None

        """
        files = self._extract_valids_files()
        if files:
            self.df = self.spark.read.text(files)
            self._prepare_dataframe(datetime_inf, datetime_sup)

    @ensure_data
    def extract_list_hosts_connected_to(self, hostname: str) -> List[str]:
        """
        Método para obtener la lista de host que se han conectado al host pasado por parámetro.
        Una vez obtenidos la lista que nos devuelve spark, lo metemos en un set, para eliminar duplicados.
        En caso de que el df no se haya cargado, se devuelve una lista con ['No hay datos']
        Args:
            hostname: nombre del host al cual queremos saber quien se ha conectado a el

        Returns: lista de hostnames conectados al hostname

        """
        return list(set([row.connector for row in self.df.where(F.col('receiver') == hostname).collect()]))  # type: ignore

    @ensure_data
    def extract_list_hosts_received_connection_to(self, hostname: str) -> List[str]:
        """
        Método para obtener la lista de host a los cuales el hostname se ha conectado.
        Una vez obtenidos la lista que nos devuelve spark, lo metemos en un set, para eliminar duplicados.
        En caso de que el df no se haya cargado, se devuelve una lista con ['No hay datos']
        Args:
            hostname: nombre del host al cual queremos saber a que hostnames se ha conectado

        Returns: lista de host donde se ha conectado el hostname

        """
        return list(set([row.receiver for row in self.df.where(F.col('connector') == hostname).collect()]))  # type: ignore

    @ensure_data
    def hostname_more_conections(self) -> str:
        """
        Método encargado de extraer el hostname que más conexiones ha realizado y ha recibido. Por optimización y dado que nuestrodataframe
        ya lo tenemos filtrado por fechas, lo que hacemos es poner todos los hostnames en una misma columna, agrupoamos por hostnames y hacemos un count y filtramos
        por el máximo, quedándonos con el máximo

        Returns: hostname con más conexiones generadas entre entrantes y salientes

        """
        df = self.df.select('receiver').union(self.df.select('connector')).withColumnRenamed('receiver', 'hostname').groupby('hostname').count()
        max = df.agg(F.max('count')).collect()[0]['max(count)']  # type: ignore
        return str(df.where(F.col('count') == max).limit(1).collect()[0].hostname)
