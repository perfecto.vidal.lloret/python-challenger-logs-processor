from .exceptions import ProcessLogException


def ensure_data(func):
    def wrapped(*args, **kwargs):
        if args[0].df is not None:
            return func(*args, **kwargs)
        raise ProcessLogException('No hay datos')

    return wrapped
