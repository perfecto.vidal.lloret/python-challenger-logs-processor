import time
from datetime import datetime, timedelta

import click

from process_log import ExtractInfo
from process_log.exceptions import ProcessLogException
from settings import SCRIPT_EVERY_SECONDS, PATH_LOGS_FILE


@click.command()
@click.option('--hostnames_connected_to', prompt='Hostnames connected to')
@click.option('--connected_to_hostname', prompt='Hostnames received connectos from')
def main(hostnames_connected_to, connected_to_hostname):
    try:
        extractor = ExtractInfo(PATH_LOGS_FILE)
        while True:
            now = datetime.utcnow()
            extractor.read_logs_and_build_dataframe(now - timedelta(hours=1), now)
            list_hostnames_connect_to = '\n'.join(extractor.extract_list_hosts_connected_to(hostnames_connected_to))
            list_connected_to_hostname = '\n'.join(extractor.extract_list_hosts_connected_to(connected_to_hostname))
            print(f"List of hostnames connected to {hostnames_connected_to}: \n{list_hostnames_connect_to}")
            print(f"List of hostnames connected to {connected_to_hostname}: \n{list_connected_to_hostname}")
            print(f"The hostname that generate most connections in the last hour is: {extractor.hostname_more_conections()}")
            time.sleep(SCRIPT_EVERY_SECONDS)
    except ProcessLogException:
        print("No hay ficheros de log, el proceso se detiene")


if __name__ == '__main__':
    main()
