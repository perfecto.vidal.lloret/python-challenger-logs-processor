import os

SCRIPT_EVERY_SECONDS = 3600  # 1 hora
PATH_LOGS_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'logs_files')  # Ruta de la carpeta que contiene los ficheros de logs

QUANTITY_PARALLEL_WRITER = 4
