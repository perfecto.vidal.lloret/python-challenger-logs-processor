import os
import random
import sys
import time
from datetime import datetime
from typing import List

from .exceptions import GeneratorException
from .settings import DEFAULT_HOSTNAMES, BE_OUT_PROBABILITY, MAX_TIME_BE_OUT


class GenerateParseLog:
    """
    Clase encargada de generación del log, para generar este log se necesita los hostnames y el path donde se guardarán los logs.
    Estos se han generado aleatoriamente y se han guardado en el setting con una lista de nombres.
    Esta decisión se toma para conseguir nombres repetidos. Estos nombres se puede generar con  librerias como "names"
    """

    def __init__(self, path: str = None, names: List[str] = None, verbose_mode: int = 0):
        """
        Constructor, encargado de cargar los nombres y guardar la ruta donde se escribiran los logs
        Args:
            path: ruta donde se escribirán los logs
            names: lista de hostnames, por defecto coge la de los settings
            verbose_mode: 0 no escribe nada (default)
                          1 si queremos que escriba por consola cada mensaje que se está escribiendo en el log
        """
        self.path = os.path.abspath(sys.argv[0]) if path is None else path
        self.names = DEFAULT_HOSTNAMES if names is None else names
        self.verbose_mode = self._validate_verbose_mode(verbose_mode)

    def _validate_verbose_mode(self, value=0) -> int:
        """
        Método encargado de validar que el verbose_mode es correcto
        Args:
            value: valor introducido por el usuario, puede ser un string o un int, con los valores 0 o 1

        Returns: el entero con el modelo de verbose

        """
        error_message = f"El verbose_mode instroducido:{value} del tipo {type(value)} es incorrecto,"
        f" debes introducir uno de los siguientes valores del tipo int o str:\n"
        f"0 si no quieres que se muestre nada\n"
        f"1 si quieres que se muestre la información que está escribiendo"

        if any([isinstance(value, tipo) for tipo in (int, str)]):
            value = int(value)
            if not any([value == valid for valid in (0, 1)]):
                raise GeneratorException(error_message)
            return value

        raise GeneratorException(error_message)

    def _timestamp_now(self) -> int:
        """
        Método privado encargado de generar el timestamp actual
        Returns: Devuelve el timestamp sin microsegundos

        """
        return int(datetime.timestamp(datetime.utcnow()))

    def _generate_name(self) -> str:
        """
        Método privado para obtener nombres de manera aleatoria
        Returns:
                str con el hostname
        """
        return self.names[random.randrange(0, len(self.names))]

    def _generate_line_log(self) -> str:
        """
        Método privado para generar la línea del log, tiene en cuenta que uno no se conecta consigo mismo
        ejemplo de return: "1576930483 Christopher_Williams Jessica_Freeman\n"

        Returns: None

        """
        host_right = host_left = self._generate_name()
        while host_right == host_left:
            host_right = self._generate_name()
        return f"{self._timestamp_now()} {host_left} {host_right}\n"

    def _is_shutted_down(self) -> bool:
        """
        Método privado para simular si está fuera de servicio o no
        :return: True or False
        """
        return random.random() <= BE_OUT_PROBABILITY

    def _write_in_file(self, line: str) -> None:
        """
        Método privado para escribir el log en el path correspondiente, al ser un log será un append
        El fichero será con el siguiente formato YYYY-MM-DD_conexion.log, es decir 2019-06-08_conexion.log
        La siguiente elección de nombre de log es debido a una facilidad a la hora de extraer los datos con spark
        Si no existe el path lo crea
        Args:
            line: linea del log del formato siguiente "1576930483 Christopher_Williams Jessica_Freeman\n"

        Returns: None

        """
        if not os.path.isdir(self.path):
            os.makedirs(self.path)

        file = os.path.join(self.path, f"{datetime.utcnow().strftime('%Y-%m-%d')}_conexion.log")
        with open(file, mode='a+') as logfile:
            logfile.write(line)
        if self.verbose_mode == 1:
            print(line)

    def _continue_process(self, datetime_stop: datetime = None) -> bool:
        """
        Método privado creado para simplificar el testeo, se encarga de decidir si continua escribiendo o no en el log
        Si el datetime_stop es None, no para nunca de escribir, hasta que se fuerza
        Args:
            datetime_stop: datetime para parar

        Returns: True or False

        """
        if datetime_stop is None:
            return True
        return datetime.now() < datetime_stop

    def run(self, datetime_stop: datetime = None) -> None:
        """
        Método que simula el comportamiento de la creación, este estará escribiendo líneas indefinidamente o cuando
        el tiempo llegue al datetime_stop.
        El sistema aplica probabilidad para simular que está fuera de servicio, también el tiempo que está fuera de servicio
        es aleatorio con un máximo establecido (5 minutos)
        Returns: None

        """
        while self._continue_process(datetime_stop):
            if self._is_shutted_down():
                time.sleep(random.randint(1, MAX_TIME_BE_OUT))
            self._write_in_file(self._generate_line_log())
