import os
import sys
import pytest  # type: ignore
from datetime import datetime
from unittest.mock import Mock, patch

import pytz

from generator_log import GenerateParseLog
from generator_log.exceptions import GeneratorException
from generator_log.settings import DEFAULT_HOSTNAMES, BE_OUT_PROBABILITY


class TestGenerateParseLog:
    def test_constructor_default(self):
        generator = GenerateParseLog()
        assert all([generator.names == DEFAULT_HOSTNAMES,
                    generator.path == os.path.abspath(sys.argv[0])])

    def test_constructor(self):
        path = 'path'
        names = ['nombre_1', 'nombre_2']
        generator = GenerateParseLog(path=path, names=names)
        assert all([generator.names == names,
                    generator.path == path])

    @patch('generator_log.generator.datetime')
    def test_dt(self, mock_dt):
        mock_dt.utcnow = Mock(return_value=datetime(2019, 12, 21, 12, 14, 43, tzinfo=pytz.utc))
        mock_dt.timestamp = datetime.timestamp
        assert GenerateParseLog()._timestamp_now() == 1576930483

    @patch('generator_log.generator.random')
    @patch('generator_log.generator.datetime')
    def test_generate_line_log_not_repeat_name(self, mock_dt, mock_random):
        mock_random.randrange = Mock(side_effect=[0, 0, 0, 0, 0, 1, 2])
        generator = GenerateParseLog(names=['perfecto', 'fernando', 'raul'])
        mock_dt.utcnow = Mock(return_value=datetime(2019, 12, 21, 12, 14, 43, tzinfo=pytz.utc))
        mock_dt.timestamp = datetime.timestamp
        assert generator._generate_line_log() == '1576930483 perfecto fernando\n'

    @pytest.mark.parametrize('value, result', [
        (BE_OUT_PROBABILITY / 10, True),
        (BE_OUT_PROBABILITY + 1, False),
        (BE_OUT_PROBABILITY, True)
    ])
    def test_is_be_out(self, value, result):
        with patch('generator_log.generator.random.random', return_value=value):
            assert GenerateParseLog()._is_shutted_down() == result

    def test_run(self):
        with patch('generator_log.generator.GenerateParseLog._continue_process', side_effect=[True, False]):
            with patch('generator_log.generator.GenerateParseLog._is_shutted_down', return_value=True):
                with patch('generator_log.generator.GenerateParseLog._write_in_file') as write:
                    with patch('generator_log.generator.time') as time:
                        GenerateParseLog().run()
        assert all([time.sleep.called,
                    write.called])

    @patch('generator_log.generator.datetime')
    def test_stop_process(self, mock_dt):
        mock_dt.now = Mock(return_value=datetime(2019, 12, 21, 12, 14, 43))
        assert all([GenerateParseLog()._continue_process(datetime(1000, 1, 1, 1, 1, 1, )) is False,
                    GenerateParseLog()._continue_process(datetime(3000, 1, 1, 1, 1, 1, )) is True,
                    GenerateParseLog()._continue_process() is True])

    def test_write_in_file(self):
        with patch('generator_log.generator.os.path.isdir', return_value=False):
            with patch('generator_log.generator.os.makedirs') as mkdir:
                with patch("builtins.open") as open:
                    GenerateParseLog(verbose_mode=1)._write_in_file("1576930483 perfecto fernando\n")
        assert all([mkdir.called,
                    open.called])

    def test_validate_verbose_mode_raise_exception(self):
        with pytest.raises(GeneratorException):
            GenerateParseLog(verbose_mode=1.1)
        with pytest.raises(GeneratorException):
            GenerateParseLog(verbose_mode='3')
