from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()
setup(
    name="python-challenger-logs-processor",
    version="1.1.0",
    author="Perfecto Vidal",
    author_email="perfecto.vidal.lloret@gmail.com",
    description="Package from challenge",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/perfecto.vidal.lloret/python-challenger-logs-processor/tree/master",
    classifiers=[
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=find_packages(exclude=['docs', 'tests', '*.tests',
                                    '*.tests.*', 'tests.*', 'tests',
                                    '.mypy*', '.tox', '.pytest*',
                                    'htmlcov*',
                                    'logs_files']),
    include_package_data=True,
    install_requires=[
        'pyspark==2.4.3',
    ],
)
