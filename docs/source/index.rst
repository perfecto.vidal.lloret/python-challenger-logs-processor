.. Python Challenger Logs Processor documentation master file, created by
sphinx-quickstart on Fri Jun  7 15:34:42 2019.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.

Welcome to Python Challenger Logs Processor's documentation!
============================================================

Requisitos previos
-------------------


Instalar:

* Python 3.7 (https://www.python.org/downloads/)

  * Instalar Java 8: En Ubuntu haríamos
       * sudo apt install openjdk-8-jdk
       * sudo update-alternatives --config java -> Seleccionamos java 8
       * verificamos la versión con java -version


Pasos para la instalación
--------------------------

* Instalar requisitos previos
* Instalar dependencias del proyecto:
    * pip install -r requirements.txt

Como simular el proceso
-----------------------
Ejecutas en dos consola distintas en la carpeta del proyecto:

* python script_to_generate_log.py
* python main.py


Métodologia de gestión de proyectos
-----------------------------------
Se ha utilizado git, en el servidor de gitlab, con la metodología de git-flow (https://danielkummer.github.io/git-flow-cheatsheet/), salvo las release, ya que
no tiene sentido utilizar una reama release, para un proyecto tan pequeño

Pasos para testear el proyecto
------------------------------

El proyecto tiene configurado el sistema tox de integración continua (https://tox.readthedocs.io/en/latest/).
Esta configurado para que pruebe en un entorno virtual con python 3.7, con los requirements
instalados.



Commando a ejecutar:

Solamente deberemos escribir "tox" en la consola. La configuración del sistema tox la encontramos en el
fichero tox.ini

¿Que hace el tox?
------------------

El procedimiento que hará al ejecutar el comando tox será:

* Crear entorno virtual y instalar los requirements

* Ejecutar los test mediante el pytest(https://docs.pytest.org/en/latest/) con la cobertura habilitada.Los resultados de la cobertura los podemos ver en la terminal o en una carpeta que se creará htmlcov, donde el fichero index.html nos mostrará una vista muy agradable para navegar por el código y ver que falta por cubrir por test unitario

* Validar la guía de estilo(Lint), mediante el flake8 (http://flake8.pycqa.org/en/latest/). Este proceso garantiza que el código cumple el PEP8(https://www.python.org/dev/peps/pep-0008/)

* Aplicar el mypy(http://mypy-lang.org/) para verificar que el tipado de nuestra librería es coherente (más información del tipado estático de python en https://www.python.org/dev/peps/pep-0484/)


Como generar el paquete instalable
-----------------------------------
Para generar el paquete wheel instalable con el gestor de paquetes de python, usaremos el fichero
setup.py que tenemos en el repositorio.
El comando para generar el paquete es:

* python setup.py bdist_wheel

Este comando generará una carpeta dist donde encontraremos el instalable. Por otro lado tendremos la carpeta
build donde podremos ver que archivos están en el paquete.

Para este caso en concreto se ha centrado en dos funcionalidades:

* generator_log, para llamar a la clase: from generator_log import GenerateParseLog

* process_log, para llamar a la clase: from process_log import ExtractInfo

Es decir el resto de ficheros se ha decidido que no se incluyera en el paquete.


Como generar la documentación automáticamente:
----------------------------------------------

Para generar la documentación automáticamente, se ha seleccionado Sphinx como
herramienta (http://www.sphinx-doc.org/en/master/). Esta documentación está en la
carpeta docs. Esta carpeta está dividida en dos partes, source y build. En source está el código en crudo
y en build estará la construcción, en nuestro caso hemos preferido una construcción basada en html por dinamismo.
Es decir la para acceder a la documentación iremos a el fichero docs/build/html/index.html.

Para construirla solamente deberemos ir a la carpeta docs y hacer make en la consola

La elección de sphinx no ha sido a la ligera, tiene una extensión llamada napoleon que nos permite generar la documentación de las
funcionalidades creadas a partir de los docstrings creados. Para hacerlo deberemos ir a la carpeta del repositorio y aplicamos

*  sphinx-apidoc -f -o docs/source "carpeta o fichero que queremos que cree la documentación"



.. toctree::
   :maxdepth: 2
   :caption: Contents:



Índices y tablas
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


