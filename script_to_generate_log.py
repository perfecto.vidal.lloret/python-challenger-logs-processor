import threading
import time

from generator_log.generator import GenerateParseLog
from settings import PATH_LOGS_FILE, QUANTITY_PARALLEL_WRITER


def thread_function() -> None:
    GenerateParseLog(PATH_LOGS_FILE).run()


def main() -> None:
    """
    Función encargada de simular la creación de los logs, estos se generar en paralelo, para simular un entorno de mucha escritura.
    El procedimiento es el siguiente, se genera el fichero con los nombres de los host y lanza en paralelo la escritura
    Toda la parametría está en el fichero de settings
    Returns: None

    """
    print("Para finalizar la creación pulse ctrl + c")
    for _ in range(QUANTITY_PARALLEL_WRITER):
        writer = threading.Thread(target=thread_function, )
        writer.start()
        time.sleep(1)


if __name__ == "__main__":
    main()
